const jwt = require('jsonwebtoken');


const createToken = (id) => {
    const payload = { id }

    return new Promise((resolve, reject) => {
        jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '10' }, (err, token) => {
            if (err) {
                reject('Token creation failed');
            } else {
                resolve(token);
            }
        });
    });
}


const createRefreshToken = (id) => {
    const payload = { id }

    return new Promise((resolve, reject) => {
        jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '7d' }, (err, refreshToken) => {
            if (err) {
                reject('Refresh token creation failed');
            } else {
                resolve(refreshToken);
            }
        });
    });
}


const validateRefreshToken = (refreshToken) => {

    return new Promise((resolve, reject) => {
        jwt.verify(refreshToken, process.env.JWT_SECRET, (err, decoded) => {
            if (decoded.expiresIn < Date.now()) {
                console.log(decoded)
                reject(true);
            } else {
                resolve(false);
            }
        });
    });
}


module.exports = {
    createToken,
    createRefreshToken,
    validateRefreshToken
}
