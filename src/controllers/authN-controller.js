const { request, response } = require('express');
const { createToken, createRefreshToken, validateRefreshToken } = require('../helpers/token-actions');
const userSchema = require('../models/user-model');
const bcrypt = require('bcrypt');


const login = async (req=request, res=response) => {
    const { email, password } = req.body;

    try {
        const user = await userSchema.findOne({ email });
        
        if (!user) {
            return res.status(400).json({
                statusCode: 400,
                message: 'User or password is incorrect'
            });
        }


        if(!user.status) {
            return res.status(400).json({
                statusCode: 400,
                message: 'User is not found'
            });
        }

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(400).json({
                statusCode: 400,
                message: 'Your credentials are incorrect'
            });
        }

        //Create token
        const [ token, newRefreshToken] = await Promise.all([
            createToken(user.id),
            createRefreshToken(user.id),
        ]);

        if(!user.refreshToken) {
            user.refreshToken = newRefreshToken;
            await user.save();
        }

        //Validate refresh token
        const isExpired = await validateRefreshToken(user.refreshToken);

        if((isExpired)) {
            //Save refresh token to user
            user.refreshToken = newRefreshToken;
            await user.save();
        }

        res.status(200).json({
            statusCode: 200,
            message: 'User successfully logged in',
            data: {
                Accesstoken: token,
                user :{
                    name: user.name,
                    email: user.email,
                    refreshToken: user.refreshToken,
                }
            }
        });

    } catch (error) {
        console.log(error);
    }
}

const register = async (req=request, res=response) => {
    const { name, email, password, role } = req.body;
}


module.exports = {
    login
}
