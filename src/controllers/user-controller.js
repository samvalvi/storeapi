const userSchema = require('../models/user-model');
const { request, response } = require('express');
const bcrypt = require('bcrypt');


const getUsers = async (req=request, res=response) => {
    
    const [total, users] = await Promise.all([
        userSchema.countDocuments({ status: true }),
        userSchema.find({ status: true })
    ]);

    const authUser = req.user;

    res.status(200).json({
        statusCode: 200,
        message: 'Users successfully retrieved',
        data: {
            "total": total,
            users
        }
    });
}


const getUserById = async (req=request, res=response) => {
    const { id } = req.params;

    const user = await userSchema.findById(id);

    res.status(200).json({
        statusCode: 200,
        message: 'User successfully retrieved',
        data: {
            user
        }
    });
}


const createUser = async (req=request, res=response) => {
    const { name, email, password, role } = req.body;

    const user = new userSchema({ name, email, password, role });

    const salt = bcrypt.genSaltSync(13)
    const hash = bcrypt.hashSync(user.password, salt)    

    user.password = hash;

    await user.save();

    res.status(201).json({
        statusCode: 201,
        message: 'User successfully created',
        data: {
            user
        }
    });
}


const updateUser = async (req=request, res=response) => {
    const { id } = req.params;
    const { role, createdAt, ...user } = req.body;

    const userToUpdate = await userSchema.findByIdAndUpdate(id, user);

    res.status(200).json({
        statusCode: 200,
        message: 'User successfully updated',
        data: {
            userToUpdate
        }
    });
}


const deleteUser = async (req=request, res=response) => {
    const { id } = req.params;

    const userToDelete = await userSchema.findByIdAndUpdate(id, {status: false});

    res.status(200).json({
        statusCode: 200,
        message: 'User successfully deleted',
        data: {
            userToDelete
        }
    });
}


module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}
