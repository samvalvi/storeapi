const userSchema = require('../models/user-model');


const validateUserByEmail = async (email) => {
    
    const userExists = await userSchema.findOne({ email });

    if (userExists) {
        throw new Error('User already exists');
    }
}


const validateUserById = async (id) => {

    const userExists = await userSchema.findById(id);

    if (!userExists) {
        throw new Error('User not found');
    }

    if(!userExists.status) {
        throw new Error('User not found');
    }
}


module.exports = {
    validateUserByEmail,
    validateUserById
}
