const { request, response } = require('express');
const { validationResult } = require('express-validator');
const roleSchema = require('../models/role-model');


const validateFields = (req=request, res=response, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            statusCode: 400,
            message: errors
        });
    }
    next();
}


const roleValidate = async (role="") => {
    const roleExists = await roleSchema.findOne({ name: role });

    if (!roleExists) {
        throw new Error('Role does not exist');
    }

}


const roleAdminValidate = (req=request, res=response, next) => {
    if(!req.user){
        return res.status(401).json({
            msg: 'Forbidden, you cannot access this resource until you verify the token.'
        });
    }
    
    const { role } = req.user;

    if(role !== 'ADMIN_ROLE')
    {
        return res.status(400).json({
            statusCode: 400,
            message: 'You are not authorized to perform this action'
        });
    }

    next();
}


const hasRole = (...roles) => {
    return (req=request, res=response, next) => {
        if(!req.user){
            return res.status(401).json({
                msg: 'Forbidden, you cannot access this resource until you verify the token.'
            });
        }
        
        const { role } = req.user;
        
        if(!roles.includes(role))
        {
            return res.status(401).json({
                statusCode: 401,
                message: 'You are not authorized to perform this action'
            });
        }

        next();
    }
}


module.exports ={ 
    validateFields,
    roleValidate,
    roleAdminValidate,
    hasRole
}
