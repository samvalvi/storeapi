const jwt = require('jsonwebtoken');
const { request, response } = require('express');
const userSchema = require('../models/user-model');


const validateToken = async (req=request, res=response, next) => {
    const token = req.headers['auth'];

    if (!token) {
        return res.status(401).json({
            statusCode: 401,
            message: 'No token provided'
        });
    }

    try {

        const { id } = jwt.verify(token, process.env.JWT_SECRET);

        const user = await userSchema.findById(id);

        req.user = user;

        next();

    }catch(error) {
        return res.status(401).json({
            statusCode: 401,
            message: 'Unauthorized'
        });
    }

}


module.exports = {
    validateToken
}
