const router = require('express').Router();
const { check } = require('express-validator');
const { getUsers, getUserById, createUser, updateUser, deleteUser } = require('../controllers/user-controller');
const { validateFields, roleValidate, roleAdminValidate, hasRole } = require('../middlewares/validate-fields');
const { validateUserById, validateUserByEmail } = require('../middlewares/validate-user');
const { validateToken } = require('../middlewares/validate-jwt');


router.get('/', [
    validateToken,
    hasRole('ADMIN_ROLE'),
], getUsers);


router.get('/:id', [
    validateToken,
    check('role').custom(roleValidate),
    hasRole('ADMIN_ROLE'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields
], getUserById);


router.post('/', [
    check('name').not().isEmpty().withMessage('Name is required'),
    check('email').isEmail().not().isEmpty().withMessage('Email is required'),
    check('email').custom(validateUserByEmail),
    check('password').not().isEmpty().withMessage('Password is required'),
    check('password').isLength({ min: 8 }).withMessage('Password must be at least 8 characters long'),
    check('role').not().isEmpty().withMessage('Role is required'),
    check('role').custom(roleValidate),
    validateFields
], createUser);


router.put('/:id', [
    validateToken,
    hasRole('ADMIN_ROLE', 'USER_ROLE'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields,
], updateUser);


router.delete('/:id', [
    validateToken,
    hasRole('ADMIN_ROLE', 'USER_ROLE'),
    check('id').not().isEmpty().withMessage('Id is required'),
    check('id').isMongoId().withMessage('Id must be a valid MongoId'),
    check('id').custom(validateUserById),
    validateFields,
],deleteUser);


module.exports = router;
