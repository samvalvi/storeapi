const router = require('express').Router();
const { validateFields } = require('../middlewares/validate-fields');
const { login } = require('../controllers/authN-controller');
const { check } = require('express-validator');


router.post('/', [
    check('email').isEmail().not().isEmpty().withMessage('Email is required'),
    check('password').not().isEmpty().withMessage('Password is required'),
    validateFields
], login);


module.exports = router;
