const mongoose = require('mongoose');
require('dotenv').config();


const options = {
    maxPoolSize: 15,
}

const getConnection = async () => {
    try{
        await mongoose.connect(process.env.MONGODB_CNN, options, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        
        console.log('Database is connected');
        
    }catch(error){
        console.log(error);
        throw new Error('Error connecting to database');
    }
}


module.exports = getConnection;
