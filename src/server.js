const express = require('express');
require('dotenv').config();
const cors = require('cors');
const getConnection = require('./database/config');


class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT || 3000;
        this.userPath = '/api/user'
        this.loginPath = '/api/login'

        this.database();

        this.middlewares();

        this.start();

    }

    middlewares(){
        this.app.use(express.json());
        this.app.use(cors());
    }

    database(){
        getConnection();
    }

    start(){
        this.app.use(this.userPath, require('./routes/users'));
        this.app.use(this.loginPath, require('./routes/authN'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log(`Server running on port ${this.port}`);
        });
    }

}

module.exports = Server;
