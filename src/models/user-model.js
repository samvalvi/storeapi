const {Schema, model} = require('mongoose');


const userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    status: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    role: {
        type: String,
        required: [true, 'Role is required'],
        enum: ['USER_ROLE', 'ADMIN_ROLE']
    },
    refreshToken: {
        type: String,
        required: false
    }
});

userSchema.methods.toJSON = function() {
    const {__v, password, _id, status, refreshToken, ...object} = this.toObject();
    object.uid = _id;
    return object;
}

module.exports = model('User', userSchema);
